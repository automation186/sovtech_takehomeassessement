package Functions.Application;
import Functions.DataFunction;
import Functions.Reports;
import Functions.Utilities;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.io.IOException;
import java.util.Properties;


public class ContactUs extends Utilities {


    DataFunction read = new DataFunction();
    private Properties prt = new Properties();
    Reports report = new Reports();
    String scrsht;



    public void contact(XSSFSheet s1, int iRow, WebDriver driver, ExtentTest test) throws Exception {

        Thread.sleep(6000);
        driver.switchTo().frame(0);
        prt = read.loadData();

        driver.findElement(By.xpath(prt.getProperty("txtName"))).sendKeys(read.getCellData("Name", iRow, s1));
        driver.findElement(By.xpath(prt.getProperty("txtemail"))).sendKeys(read.getCellData("WorkEmail", iRow, s1));
        driver.findElement(By.xpath(prt.getProperty("txtContactNumber"))).sendKeys(read.getCellData("ContactNumber", iRow, s1));
        Thread.sleep(6000);


        String companyValue =read.getCellData("CompanySize", iRow, s1);

        if(!companyValue.equals("")){
            Select Companysize = new Select( driver.findElement(By.xpath(prt.getProperty("drpCompanySize"))));
            Companysize.selectByVisibleText(read.getCellData("CompanySize", iRow, s1));
        }

        Thread.sleep(6000);
        Select service= new Select( driver.findElement(By.xpath(prt.getProperty("drpService"))));
        service.selectByVisibleText(read.getCellData("Service", iRow, s1));
        Thread.sleep(2000);


        driver.findElement(By.xpath(prt.getProperty("chkConsent"))).click();

        scrsht=report.takeScreenshort(".\\Screenshorts\\", driver);
        test.pass("Captured Contact Details", MediaEntityBuilder.createScreenCaptureFromPath("."+scrsht).build());


        driver.findElement(By.xpath(prt.getProperty("btnSubmit"))).click();
        Thread.sleep(2000);



        String succefulSubmit ="//h2[text()='Submission Successful']";
        String expectedMessage;
        if(driver.findElement(By.xpath(succefulSubmit)).isDisplayed()){

            expectedMessage = driver.findElement(By.xpath(succefulSubmit)).getText();

            if (expectedMessage.equals("Submission Successful")) {

                System.out.print("Sucessfully submitted the form");
                Thread.sleep(2000);
                scrsht=report.takeScreenshort(".\\Screenshorts\\", driver);
                test.pass("Sucessfully submitted the form", MediaEntityBuilder.createScreenCaptureFromPath("."+scrsht).build());



            }

        }

        else{

            System.out.print("Submission failed");
            Thread.sleep(2000);
            scrsht=report.takeScreenshort(".\\Screenshorts\\", driver);
            test.fail("Failed to submit the form", MediaEntityBuilder.createScreenCaptureFromPath("."+scrsht).build());


        }







    }
}
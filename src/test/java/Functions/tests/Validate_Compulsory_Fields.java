package Functions.tests;

import Functions.Application.ContactUs;
import Functions.Reports;
import Functions.Utilities;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;

public class Validate_Compulsory_Fields {

    private int count = 2;
    WebDriver driver;
    ContactUs cont = new ContactUs();
    Utilities util = new Utilities();
    Reports report = new Reports();

    ExtentReports extent;
    ExtentTest test;

    @BeforeClass
    public void setUpBeforeTest() throws IOException {

        driver= util.SatartupBrowser();

    }



    @Test
    public void test() throws Exception {



        XSSFWorkbook workbook= new XSSFWorkbook("testData\\ContactUs.xlsx");

        XSSFSheet sheetname = workbook.getSheet("ContactUs");

        extent = report.ExtentReportMethod("./Reports/SolveTech.html");
        test = report.ExtentTestMethod(extent);





        try {


            cont.contact(sheetname,count,driver,test);



            workbook.close();





        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }


    @AfterClass
    public void quitBrowser(){
        extent.flush();
        driver.quit();

    }
}


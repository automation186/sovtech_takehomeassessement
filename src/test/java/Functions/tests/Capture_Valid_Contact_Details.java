package Functions.tests;

import Functions.Application.ContactUs;

import Functions.Reports;
import Functions.Utilities;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;

import java.io.IOException;



public class Capture_Valid_Contact_Details {
    private int count = 1;
    WebDriver driver;
    ContactUs cont = new ContactUs();
    Utilities util = new Utilities();
    Reports report = new Reports();

    ExtentReports extent;
    ExtentTest test;

    @BeforeClass
    public void setUpBeforeTest() throws IOException {

        driver= util.SatartupBrowser();


    }



    @Test
    public void test() throws Exception {



        try {

            Thread.sleep(3000);

            XSSFWorkbook workbook= new XSSFWorkbook("testData\\ContactUs.xlsx");

            XSSFSheet sheetname = workbook.getSheet("ContactUs");

            extent = report.ExtentReportMethod("./Reports/SolveTech.html");
            test = report.ExtentTestMethod(extent);

            Thread.sleep(5000);
            cont.contact(sheetname,count,driver,test);



            workbook.close();





        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }


    @AfterClass
    public void quitBrowser(){


        driver.quit();
        extent.flush();

    }


}

package Functions;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class Utilities {

    WebDriver driver;
    String browserName ="CHROME";
    String url= "https://www.sovtech.co.za/contact-us/";


    public WebDriver SatartupBrowser() {
        System.out.println("Browser is running on :" + browserName);

        if (browserName.equalsIgnoreCase("CHROME")) {

            WebDriverManager.chromedriver().setup();

            driver = new ChromeDriver();
            driver.manage().window().maximize();
            driver.get(url);


        }

        else if (browserName.equalsIgnoreCase("EDGE")) {

            System.setProperty("webdriver.edge.driver","C:\\Mywork\\drivers\\msedgedriver.exe");
            EdgeOptions edgeOptions=new EdgeOptions();
            driver=new EdgeDriver(edgeOptions);

        }



        else {
            System.out.println("invalid browser");
        }

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
       // prt = load.loadData();

        driver.get(url);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
       return driver;

    }

    public void ExtentLogPass(WebDriver driver, String sMessagePass, ExtentTest logger, Boolean Screenshot, String sDefaultPath) throws Exception {
        if (Screenshot) {

            String fileName = takeScreenShot(driver, sMessagePass, sDefaultPath);
            logger.pass(sMessagePass, MediaEntityBuilder.createScreenCaptureFromBase64String(fileName).build());


        } else {
            logger.pass(sMessagePass);
        }
    }

    public String takeScreenShot(WebDriver driver, String FileName, String sDefaultPath) throws Exception {

        String encodedBase64 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BASE64);
        return encodedBase64;
    }

}

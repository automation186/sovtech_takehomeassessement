package Functions;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;

import java.io.File;
import java.io.IOException;
import java.util.Properties;
import java.io.FileInputStream;


public class DataFunction {
    Properties propertyobj = new Properties();

    public String getCellData (String strColumn, int iRow, XSSFSheet sheet) {

        String sValue = null;

        XSSFRow row =sheet.getRow(0);
        for (int i = 0; i < columnCount(sheet); i++) {

            if(row.getCell(i).getStringCellValue().trim().equals(strColumn)){

                XSSFRow raw =sheet.getRow(iRow);
                XSSFCell cell = raw.getCell(i);
                DataFormatter formater = new DataFormatter();
                sValue=formater.formatCellValue(cell);

            }
        }

        return sValue;
    }

    public int columnCount(XSSFSheet sheet){

        return sheet.getRow(0).getLastCellNum();
    }

    public Properties loadData() throws IOException {

        File path = new File("Repository/ContactUs.properties");
        FileInputStream fis = new FileInputStream(path);
        propertyobj.load(fis);

        return propertyobj;

    }




}

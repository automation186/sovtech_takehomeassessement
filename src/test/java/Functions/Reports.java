package Functions;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Reports {
    ExtentTest  test;

    public ExtentReports ExtentReportMethod(String Report) {

        ExtentHtmlReporter htmlReport = new ExtentHtmlReporter(Report);
        ExtentReports extent = new  ExtentReports();

        String css= ".r-img {width: 60%;}";
        htmlReport.config().setCSS(css);
        htmlReport.config().setTheme(Theme.STANDARD);

        htmlReport.setAppendExisting(true);

        extent.attachReporter(htmlReport);
        return extent;



    }


    public ExtentTest ExtentTestMethod(ExtentReports extent) {

        test= extent.createTest("SovTech Contact Us");

        test.assignAuthor("Refiwe Ceba");
        return test;

    }

    // Method for screenshorts
    public String takeScreenshort(String filename, WebDriver driver) throws IOException {



        File schreenshort =((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);

        String timStamp = new SimpleDateFormat("yyyMMdd_HHss").format(Calendar.getInstance().getTime());

        filename =filename+timStamp+".png";
        FileUtils.copyFile(schreenshort, new File(filename));

        return filename;

    }

}
